# Gitlab Continous Integration Sample C++ Project

This is a quick C++ project to demonstrate how to use Continuous Integration on GitLab. You clone this repository locally (or import it on GitLab), and when you push it to GitLab, the .gitlab-ci.yml file will trigger a runner to run the verify.sh test.